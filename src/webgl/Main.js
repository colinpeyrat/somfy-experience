import { GUI } from 'dat.gui/build/dat.gui.js';
import * as THREE from 'three';
const OrbitControls = require('three-orbit-controls')(THREE);
import Loader from 'webgl/utils/Loader';
import Light from 'webgl/Light';

class Main {
  constructor({ canvas, onLoaded, onLoad }) {
    this.canvas = canvas;

    this.onLoaded = onLoaded;
    this.onLoad = onLoad;

    this.renderer = new THREE.WebGLRenderer({
      antialias: true,
      canvas: this.canvas
    });
    this.renderer.shadowMap.enabled = true;

    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color(0xffad0c);
    this.scene.fog = new THREE.FogExp2(0xffad0c, 0.0025);

    this.camera = new THREE.PerspectiveCamera(
      75,
      window.innerWidth / window.innerHeight,
      0.1,
      1000
    );

    this.camera.position.set(0, 0, -7.5);
    this.camera.lookAt(new THREE.Vector3());

    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
    this.controls.autoRotate = true;
    this.controls.autoRotateSpeed = -2.0;

    this.params = {
      cubeSpeed: 0.02,
      pulseSpeed: 0.025
    };

    this.uniforms = {
      u_time: { type: 'f', value: 1.0 }
    };
  }

  init() {
    this.createLights();
    this.createGround();

    this.onWindowResize();
    this.bindEvents();

    if (process.env.NODE_ENV !== 'production') {
      this.initGui();
    }

    this.animate();
  }

  loadModel() {
    this.createPoto();
  }

  createLights() {
    this.Light = new Light({
      scene: this.scene,
      camera: this.camera
    });
  }

  createGround() {
    const groundGeo = new THREE.PlaneBufferGeometry(10000, 10000);
    const groundMat = new THREE.MeshPhongMaterial({
      color: 0xffad0c,
      specular: 0xffad0c,
      shininess: 0.1
    });

    const ground = new THREE.Mesh(groundGeo, groundMat);
    ground.rotation.x = -Math.PI / 2;
    ground.position.y = -19.5;

    this.scene.add(ground);

    ground.receiveShadow = true;
  }

  createPoto() {
    this.Loader = new Loader();

    this.Loader.load({
      url: 'poto.obj',
      urlMtl: 'poto.mtl',
      onLoaded: object => {
        const { max, min } = new THREE.Box3().setFromObject(object);
        const cameraMaterial = new THREE.MeshPhongMaterial({
          color: 0x000000,
          shininess: 100
        });

        object.traverse(child => {
          if (child instanceof THREE.Mesh) {
            child.material[0] = cameraMaterial;
            child.castShadow = true;
            child.receiveShadow = true;
          }
        });

        object.rotation.y = -Math.PI / 2;
        object.position.y = -((max.y - min.y) / 2);

        object.castShadow = true;
        object.receiveShadow = true;

        this.scene.add(object);

        this.isModelLoaded = true;

        this.onLoaded();
      },
      onLoad: this.onLoad
    });
  }

  showModel() {
    const destination = new THREE.Vector3(0, 20, -40);

    TweenMax.to(this.camera.position, 2, {
      ...destination,
      ease: Quart.easeOut,
      onUpdate: () => {
        this.camera.lookAt(new THREE.Vector3());
      }
    });
  }

  bindEvents() {
    window.addEventListener('resize', this.onWindowResize.bind(this), false);
  }

  onWindowResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }

  initGui() {
    const gui = new GUI();
    return gui;
  }

  animate() {
    requestAnimationFrame(this.animate.bind(this));

    if (this.shouldRotate) {
      this.controls.update();
    }

    this.render();
  }

  render() {
    this.renderer.render(this.scene, this.camera);
  }
}

export default Main;
