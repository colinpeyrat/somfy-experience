import * as THREE from 'three';
import OBJLoader from 'imports-loader?THREE=three!exports-loader?THREE.OBJLoader!three-extras/loaders/OBJLoader.js';
import MTLLoader from 'imports-loader?THREE=three!exports-loader?THREE.MTLLoader!three-extras/loaders/MTLLoader.js';

class Loader {
  constructor() {
    this.mtlLoader = new MTLLoader();

    this.baseUrl =
      process.env.NODE_ENV === 'production'
        ? '/maika/static/models/'
        : '/static/models/';
  }
  load({ url, urlMtl, onLoaded, onLoad, onError }) {
    url = this.baseUrl + url;
    urlMtl = this.baseUrl + urlMtl;

    this.mtlLoader.load(urlMtl, materials => {
      materials.preload();

      const objLoader = new OBJLoader();

      objLoader.setMaterials(materials);
      objLoader.load(url, onLoaded, onLoad, onError);
    });
  }
}

export default Loader;
