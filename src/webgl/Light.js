import * as THREE from 'three';

class Light {
  constructor({ scene, camera }) {
    this.scene = scene;
    this.camera = camera;

    this.initLight();
  }

  initLight() {
    const ambientLight = new THREE.AmbientLight(0xffffff, 0.8);
    this.scene.add(ambientLight);

    const pointLight = new THREE.PointLight(0xffffff, 0.8);
    this.camera.add(pointLight);

    this.directionalLight = new THREE.DirectionalLight(0xffffff, 0.2);
    this.directionalLight.position.set(-75, 200, -100);
    this.scene.add(this.directionalLight);

    this.directionalLight.castShadow = true;

    this.directionalLight.shadow.mapSize.width = 2048;
    this.directionalLight.shadow.mapSize.height = 2048;

    const d = 50;
    this.directionalLight.shadow.camera.left = -d;
    this.directionalLight.shadow.camera.right = d;
    this.directionalLight.shadow.camera.top = d;
    this.directionalLight.shadow.camera.bottom = -d;
    this.directionalLight.shadow.camera.far = 3500;
    this.directionalLight.shadow.bias = -0.0001;
  }
}

export default Light;
